(function($) {
    "use strict";

    $(document).ready(function() {

        /*=========================================================================
         ===  countdown
         ========================================================================== */
        if ( $('#lgx-countdown').length ) {

            var dataTime = $('#lgx-countdown').data('date'); // Date Format : Y/m/d

            $('#lgx-countdown').countdown(dataTime, function(event) {
                var $this = $(this).html(event.strftime(''
                    /*+ '<span class="lgx-weecks">%w <i> weeks </i></span> '*/
                    + '<span class="lgx-days">%D <i> Days </i></span> '
                    + '<span class="lgx-hr">%H <i> Hours </i></span> '
                    + '<span class="lgx-min">%M <i> Minutes </i></span> '
                    + '<span class="lgx-sec">%S <i> Seconds </i></span>'
                ));
            });
        }

        if($('#lgx-typed-center').length){
            $('#lgx-typed-center').typed({
                strings: ["World ","Juice Brands ", "Devices ","Manufacturers ","Distributors ","Retailers "],
                // typing speed
                typeSpeed: 100,
                // time before typing startsManufacturers
                startDelay: 0,
                // backspacing speed
                backSpeed: 0,
                // shuffle the strings
                shuffle: false,
                // time before backspacing
                backDelay: 500,
                // loop
                loop: true,
                // false = infinite
                loopCount: false,
                // show cursor
                showCursor: true,
                // character for cursor
                cursorChar: "|",
                // either html or text
                contentType: 'html'
            });
        }

        // TABEL

        $('tr.header').click(function(){

            // $(this).nextUntil('tr.header').slideToggle(1000);
            $(this).nextUntil('tr.header').css('display', function(i,v){
                return this.style.display === 'table-row' ? 'none' : 'table-row';
            });
            $(this).find('span').text(function(_, value) {
                return value == '+' ? '-' : '+'
              });
        });


        $('tr.header1').click(function(){

            // $(this).nextUntil('tr.header').slideToggle(1000);
            $(this).nextUntil('tr.header1').css('display', function(i,v){
                return this.style.display === 'table-row' ? 'none' : 'table-row';
            });
            $(this).find('span').text(function(_, value) {
                return value == '+' ? '-' : '+'
              });
        });

                $("#telephone").intlTelInput({
                        separateDialCode:true,

                });

                $("#telephone1").intlTelInput({
                    separateDialCode:true,
            });




             


    });//DOM READY


})(jQuery);
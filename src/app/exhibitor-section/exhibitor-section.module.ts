import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ExhibitorSectionComponent } from './exhibitor-section.component';
import { RouterModule } from '@angular/router';
import { FormsModule,ReactiveFormsModule }   from '@angular/forms';
import { TooltipModule } from 'ngx-bootstrap';
import { NgxSpinnerModule } from "ngx-spinner";

import {
  MatInputModule,
  MatPaginatorModule,
  MatProgressSpinnerModule,
  MatSortModule,
  MatTableModule,
  MatSelectModule,
  MatMenuModule,
  MatProgressBarModule,
  MatButtonModule,
  MatCheckboxModule,
  MatDialogModule,
  MatTabsModule,
  MatNativeDateModule,
  MatCardModule,
  MatRadioModule,
  MatIconModule,
  MatDatepickerModule,
  MatExpansionModule,
  MatAutocompleteModule,
  MAT_DIALOG_DEFAULT_OPTIONS,
  MatSnackBarModule,
  MatTooltipModule,
  MatSlideToggleModule,
} from '@angular/material';
import {MatStepperModule} from '@angular/material/stepper';
import { ToastrModule } from 'ngx-toastr';
import {AngularStickyThingsModule} from '@w11k/angular-sticky-things';
import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';
import { NgImageSliderModule } from 'ng-image-slider';
import { NgxMatIntlTelInputModule} from 'ngx-mat-intl-tel-input';
import { NginitDirective } from './nginit.directive';

@NgModule({
  declarations: [ExhibitorSectionComponent, NginitDirective],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: ExhibitorSectionComponent
      },
    ]),
    AngularStickyThingsModule,
    FormsModule,
    ReactiveFormsModule,
    ToastrModule.forRoot(),
    TooltipModule.forRoot(),
    ScrollToModule.forRoot(),
    MatButtonModule,
    MatMenuModule,
    MatSelectModule,
    MatInputModule,
    MatTableModule,
    MatAutocompleteModule,
    MatRadioModule,
    MatIconModule,
    MatNativeDateModule,
    MatProgressBarModule,
    MatDatepickerModule,
    MatCardModule,
    MatPaginatorModule,
    MatSortModule,
    MatCheckboxModule,
    MatProgressSpinnerModule,
    MatSnackBarModule,
    MatExpansionModule,
    MatTabsModule,
    MatTooltipModule,
    MatDialogModule,
    MatSlideToggleModule,
    NgImageSliderModule,
    MatStepperModule,
    NgxMatIntlTelInputModule,
    NgxSpinnerModule
  ],
  exports:[NginitDirective]
})
export class ExhibitorSectionModule { }

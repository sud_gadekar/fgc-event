import { Directive, EventEmitter, Output, OnInit } from '@angular/core';

@Directive({
  selector: '[appNginit]'
})
export class NginitDirective implements OnInit {
  @Output()
  appNginit: EventEmitter<any> = new EventEmitter();

  constructor() {
  
   }
   ngOnInit() {
    this.appNginit.emit();
  }
  
}

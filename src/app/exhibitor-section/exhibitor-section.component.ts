import { Component, OnInit,ChangeDetectorRef,AfterViewInit,HostListener,Input, NgZone } from '@angular/core';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { MatDialog } from '@angular/material';
import { DataService } from '../shared-services/data.service';
import * as moment from 'moment';
declare var $:any;
//declare var Razorpay:any;
declare var CollectJS:any;

var unserialize = require('locutus/php/var/unserialize');
declare var PerspT: any;



@Component({
  selector: 'app-exhibitor-section',
  templateUrl: './exhibitor-section.component.html',
  styleUrls: ['./exhibitor-section.component.css']
})
export class ExhibitorSectionComponent implements OnInit {

  rescom = false;
	placeholderDetails=[];
	corners = [];
  placehoders;
	width;
	height;
  size = {width: 150, height: 120,space:0};
  hallData;
  exhibitors;


  constructor(		private pageService: DataService, 
		private ref:ChangeDetectorRef, 
    private router:Router,
    private activatedRoute: ActivatedRoute,
		public dialog: MatDialog,
    private toastr: ToastrService) {
        
   }


  ngOnInit() {

    // CollectJS.configure(this.paymentOptions);

   


    this.pageService.getRecordsNoFilter('eventhall?event_id=7').subscribe(res =>
      {
        if (res.Data != '' && res.Data)
        {
          this.hallData=res.Data;
         for(let j=0; j<res.Data.length;j++)
         {
          
          this.pageService.getRecordsByid('hallplaceholder', res.Data[j].id).subscribe(data =>
            {
              if(data.Data!=null && data.Data!='')
              {
                this.placehoders=data.Data;
                this.hallData[j].placeholderDetails = [];
                for (let i = 0; i < data.Data.length; i++) {
                  this.hallData[j].placeholderDetails.push(data.Data[i]);
                }
                this.pageService.getRecordsNoFilter('exhibitor').subscribe(res=>
                  {
                    if(res.Data !=null && res.Data!='' && this.placehoders)
                    {
                      this.exhibitors=res.Data;
                      for(let i=0;i<this.placehoders.length;i++)
                    {
                      // this.placehoders[i]["seat_no"] = '';
                      this.placehoders[i]["exhibitor_id"] = '';
                      this.placehoders[i]["active"] = '';
                        for(let j=0; j<res.Data.length; j++)
                        {
                          if(this.placehoders[i].seat_no==res.Data[j].booked_seat )
                          {
                            // this.placehoders[i]["seat_no"] = res.Data[j].booked_seat;
                            this.placehoders[i]["exhibitor_id"] = res.Data[j].id;
                            this.placehoders[i]['unavailable'] = 'unavailable';
                          }
                        }
                    }
                    } 
                    console.log(this.placehoders)
                  })
                  
                this.rescom = true;
                this.ref.markForCheck();
              }
            });
         }
        }
        else
        {
          this.toastr.error('Hall data not found', 'Error', {
            timeOut: 3000,
            progressBar: true,
            closeButton: true
          });
        }
        this.ref.markForCheck();
      });

setInterval(() =>
{
this.ref.markForCheck();
}, 500);
   
  }
  
	transform2d(elt, x1, y1, x2, y2, x3, y3, x4, y4)
  {
    var w = elt.offsetWidth, h = elt.offsetHeight;
    var transform = PerspT([0, 0, w, 0, 0, h, w, h], [x1, y1, x2, y2, x3, y3, x4, y4]);
    var t = transform.coeffs;
    t = [t[0], t[3], 0, t[6],
         t[1], t[4], 0, t[7],
         0   , 0   , 1, 0   ,
         t[2], t[5], 0, t[8]];

    t = "matrix3d(" + t.join(", ") + ")";
    elt.style["-webkit-transform"] = t;
    elt.style["-moz-transform"] = t;
    elt.style["-o-transform"] = t;
    elt.style.transform = t;
    this.ref.markForCheck();
  }

updatePlaceholder(event,cor,img)
{
 this.width = $('.maindiv').width();
 this.height = $('.maindiv').height();
  const unserialize_cor  = unserialize(cor);
  let PointInPx = [];
  unserialize_cor.forEach(point =>
  {
    PointInPx.push(this.getPointInPx(point));
  });
  
  this.corners = [ PointInPx[0][0], PointInPx[0][1], PointInPx[1][0], PointInPx[1][1], PointInPx[3][0], PointInPx[3][1], PointInPx[2][0], PointInPx[2][1] ]

  if(event)
  {
    this.transform2d(event, this.corners[0], this.corners[1], this.corners[2], this.corners[3],
                 this.corners[4], this.corners[5], this.corners[6], this.corners[7]);
  }

}

getPointInPx(point)
{
  return [
          this.xToPx(point[0]),
          this.yToPx(point[1])
      ];
}

xToPx(point)
{
   return point * this.width / 100;
}

yToPx(point)
{
  return point * this.height / 100;
}
bookSeat(hallData,j,i)
  {
    if (hallData != '' && hallData != null)
    {
     console.log(hallData)
    }
  }
}

import { Component } from '@angular/core';
import {FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';
import { DataService } from './shared-services/data.service';
import * as moment from 'moment';
import { ToastrService } from 'ngx-toastr';
import { Event, NavigationCancel, NavigationEnd, RouteConfigLoadStart, NavigationStart, RouteConfigLoadEnd, Router } from '@angular/router';
import { LoadingBarService } from '@ngx-loading-bar/core';

declare var $:any;





@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  // title = 'cbd-world';
  today: number = Date.now();

  contactForm:FormGroup;
  contact_loader=false;



  constructor(
              private fb:FormBuilder,
              private service:DataService,
              private toastr: ToastrService,
              public router: Router,
              public loader: LoadingBarService,
              )
            {
              this.router.events.subscribe(event => {
                if (event instanceof NavigationStart) {
                  // set page progress bar loading to start on NavigationStart event router
                  this.loader.start();
                }
                if (event instanceof RouteConfigLoadStart) {
                  this.loader.increment(35);
                }
                if (event instanceof RouteConfigLoadEnd) {
                  this.loader.increment(75);
                }
                if (event instanceof NavigationEnd || event instanceof NavigationCancel) {
                  // set page progress bar loading to end on NavigationEnd event router
                  this.loader.complete();
                }
              });
            }

  ngOnInit()
  {

    $.getScript('assets/js/count_down.js');

    this.contactForm=this.fb.group({
      name:['', Validators.required],
      last_name:[''],
      email:['', [Validators.required,Validators.email]],
      subject:['', Validators.required],
      message:['', Validators.required],
      type:['contact']
    })
  }
  onSubmit()
  {
    //console.log(this.contactForm.value);
    if(this.contactForm.invalid)
    {
      return false;
    }
    this.contact_loader=true;
     this.service.postRecord('mailsend',this.contactForm.value).subscribe(res => {
      if(res.Status)
      {
        this.toastr.success(res.Msg, 'Success', {
          timeOut: 3000,
          progressBar: true,
          closeButton: true
        });
      }
      else {
        this.toastr.error(res.Msg, 'Error', {
          timeOut: 3000,
          progressBar: true,
          closeButton: true
        });
      }
      this.contact_loader=false;
      this.contactForm.reset();
     });
  }
}

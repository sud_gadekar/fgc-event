import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';


@Component({
  selector: 'app-register-success',
  templateUrl: './register-success.component.html',
  styleUrls: ['./register-success.component.css']
})
export class RegisterSuccessComponent implements OnInit {

  sucessMessage; 

  constructor(private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    window.scrollTo(0, 1);
    this.activatedRoute.queryParams.subscribe(params => {
      const userType = params['type'];
      console.log(userType);
      
      if(userType == 'exhibitor'){
        this.sucessMessage = "Thank you for registering as an exhibitor at CBD + Vape World! We've shared the details on your email ID.";
      }
      else if(userType == 'attendee'){
        this.sucessMessage = "Thank you for registering for the premier CBD + Vape World! We've shared the details on your email ID.";
      }
    });
  }

  

}

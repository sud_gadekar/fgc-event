import { Component, OnInit, ChangeDetectorRef, NgZone, ViewChild } from '@angular/core';
import {FormBuilder, FormGroup, Validators, FormControl, FormArray} from '@angular/forms';
import { DataService } from '../shared-services/data.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-attendee-booking',
  templateUrl: './attendee-booking.component.html',
  styleUrls: ['./attendee-booking.component.css']
})
export class AttendeeBookingComponent implements OnInit {

  Attendee_registration:FormGroup;
  panelOpenState = false;

  @ViewChild('captchaRef', {
    static: false
}) captchaRef: any;

  constructor( private FB: FormBuilder,
               private service:DataService,
               private toastr: ToastrService,
               private router:Router,
               private fb:FormBuilder,
               private ref:ChangeDetectorRef,
               private spinner: NgxSpinnerService
              ) {
                
              }


  ngOnInit()
  {
    window.scrollTo(0, 1);
    this.Attendee_registration = this.FB.group(
	    {
	        first_name: ['', Validators.required],
	        company_name: ['', Validators.required],
	        email: ['', [Validators.email, Validators.required]],
	        password: ['', [Validators.required, Validators.minLength(6)]],
          confirm_pwd: ['',Validators.required],
          event_id:[17],
          plan_id:23,
          accept_terms:[0, Validators.required],
          event_source: ['', Validators.required]
	    },
	    {
	        validator: this.mustMatch
	    });
  }


  mustMatch(group:FormGroup){
    let passControl = group.get('password');
    let confirmPassControl = group.get('confirm_pwd');
    if (confirmPassControl.value)
    {
      if(passControl.value === confirmPassControl.value) {
        if(confirmPassControl.hasError('notSame')){
          delete confirmPassControl.errors['notSame'];
          confirmPassControl.updateValueAndValidity();
        }
        return null;
      } else {
        confirmPassControl.setErrors({notSame: true});
        return { notSame: true };
      } 
    }
  }

  isControlHasError(controlName: string, validationType: string): boolean
  {
    const control = this.Attendee_registration.controls[controlName];
    if (!control) {
      return false;
    }
    const result = control.hasError(validationType) && (control.dirty || control.touched);
    return result;
  }

  resolved(captchaResponse: string, res) {
    console.log(`Resolved response token: ${captchaResponse}`);
     //await this.sendTokenToBackend(captchaResponse);
  }

  onSubmit()
    {
      let strValue = this.Attendee_registration.controls['accept_terms'].value.toString();
      this.Attendee_registration.patchValue({accept_terms:strValue});

	        this.service.postRecord('attendee', this.Attendee_registration.value).subscribe(res =>
	        {
	            if (res.Status)
	            {
	                this.toastr.success(res.Msg, 'Success',
	                {
	                    timeOut: 3000,
	                    progressBar: true,
	                    closeButton: true
                  });
                  this.Attendee_registration.reset();
	            }
	            else
	            {
	                this.toastr.error(res.Msg, 'Error',
	                {
	                    timeOut: 3000,
	                    progressBar: true,
	                    closeButton: true
	                });
	            }
	        })
    }
}

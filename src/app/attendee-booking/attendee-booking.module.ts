import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule,ReactiveFormsModule }   from '@angular/forms';
import { CommonModule } from '@angular/common';
import { AttendeeBookingComponent } from './attendee-booking.component';
import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';
import {
  MatInputModule,
  MatPaginatorModule,
  MatProgressSpinnerModule,
  MatSortModule,
  MatTableModule,
  MatSelectModule,
  MatMenuModule,
  MatProgressBarModule,
  MatButtonModule,
  MatCheckboxModule,
  MatDialogModule,
  MatTabsModule,
  MatNativeDateModule,
  MatCardModule,
  MatRadioModule,
  MatIconModule,
  MatDatepickerModule,
  MatExpansionModule,
  MatAutocompleteModule,
  MAT_DIALOG_DEFAULT_OPTIONS,
  MatSnackBarModule,
  MatTooltipModule,
  MatSlideToggleModule,
} from '@angular/material';

import {MatStepperModule} from '@angular/material/stepper';
import { NgxSpinnerModule } from "ngx-spinner";

import { NgxMatIntlTelInputModule} from 'ngx-mat-intl-tel-input';
// import { IntlInputPhoneModule } from 'intl-input-phone';
import { NgxIntlTelInputModule} from 'ngx-intl-tel-input'
import { InternationalPhoneNumberModule } from 'ngx-international-phone-number';
import { RecaptchaModule, RecaptchaFormsModule } from 'ng-recaptcha';


@NgModule({
  declarations: [AttendeeBookingComponent],
  imports: [
    CommonModule,
   FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild([
      {
        path: '',
        component: AttendeeBookingComponent
      },
    ]),
  ScrollToModule.forRoot(),
      MatButtonModule,
    MatMenuModule,
    MatSelectModule,
    MatInputModule,
    MatTableModule,
    MatAutocompleteModule,
    MatRadioModule,
    MatIconModule,
    MatNativeDateModule,
    MatProgressBarModule,
    MatDatepickerModule,
    MatCardModule,
    MatPaginatorModule,
    MatSortModule,
    MatCheckboxModule,
    MatProgressSpinnerModule,
    MatSnackBarModule,
    MatExpansionModule,
    MatTabsModule,
    MatTooltipModule,
    MatDialogModule,
    MatSlideToggleModule,
    MatStepperModule,
    NgxIntlTelInputModule,
    NgxMatIntlTelInputModule,
    InternationalPhoneNumberModule,
    NgxSpinnerModule,
    RecaptchaModule, 
    RecaptchaFormsModule,
  ]
})
export class AttendeeBookingModule { }

import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { NgImageSliderModule } from 'ng-image-slider';


@NgModule({
  imports: [
  
    CommonModule,
    NgImageSliderModule,

  ],
  providers: [],
  declarations: [ ]
})
export class HomePageModule { }

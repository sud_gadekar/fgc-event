import { Component, OnInit } from '@angular/core';
declare var $:any;

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {

  public imageObject = [{
    image: 'assets/img/slider/six.png',
    thumbImage: 'assets/img/slider/six.png',
    title: '6 SqM '
  },
  {
    image: 'assets/img/slider/nine.png',
    thumbImage: 'assets/img/slider/nine.png',
    title: '9 SqM'
  },
  {
    image: 'assets/img/slider/tweleve.png',
    thumbImage: 'assets/img/slider/tweleve.png',
    title: '12 SqM '
  },
  {
    image: 'assets/img/slider/eighteen.png',
    thumbImage: 'assets/img/slider/eighteen.png',
     title: '18 SqM'
  },
  {
    image: 'assets/img/slider/twentyfour.png',
    thumbImage: 'assets/img/slider/twentyfour.png',
    title: '24 SqM'
  },
  {
    image: 'assets/img/slider/thirtysix.png',
    thumbImage: 'assets/img/slider/thirtysix.png',
    title: '36 SqM'
  },
];

public partnerImage = [
  {
    image: 'assets/img/ovation.png',
    thumbImage: 'assets/img/ovation.png',
   },
  {
   image: 'assets/img/huddle_logo.svg',
   thumbImage: 'assets/img/huddle_logo.svg',
  },
  {
    image: 'assets/img/ae2.png',
    thumbImage: 'assets/img/ae2.png',
   },
   {
    image: 'assets/img/mtn.png',
    thumbImage: 'assets/img/mtn.png',
   },
   {
    image: 'assets/img/ibtc.png',
    thumbImage: 'assets/img/ibtc.png',
   }
]
  submitted = false;
  sponsorsImg=["https://placehold.it/467x157","https://placehold.it/467x157","https://placehold.it/467x157"
  ];

  constructor() {
   }

  ngOnInit() {
    window.scrollTo(0, 1);
    $.getScript('assets/js/count_down.js');  
  }
}

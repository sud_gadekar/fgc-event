import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomePageComponent } from './home-page/home-page.component';
import { RegisterSuccessComponent } from './register-success/register-success.component';
import {AttendeeLoginComponent} from './attendee-login/attendee-login.component';
import { SponsorsComponent } from './sponsors/sponsors.component';
const routes: Routes = [{
                          path:'', 
                          component:HomePageComponent
                        },
                        {
                          path:'register-success', 
                          component:RegisterSuccessComponent
                        },
                        
                        {
                          path:'exhibitor-booking', 
                          loadChildren:'../app/exhibitor-section/exhibitor-section.module#ExhibitorSectionModule'
                        },   
                        {
                          path:'exhibitor-book', 
                          loadChildren:'../app/exhibitor-booking/exhibitor-booking.module#ExhibitorBookingModule'
                        },  
                        {
                          path:'attendee-booking', 
                          loadChildren:'../app/attendee-booking/attendee-booking.module#AttendeeBookingModule'
                        },
                        {
                          path:'attendee-login', 
                          component:AttendeeLoginComponent
                        },
                        {
                          path:'sponsor', 
                          component:SponsorsComponent
                        },
                      ];

@NgModule({
  imports: [
  RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { Component, OnInit,ChangeDetectorRef,AfterViewInit,HostListener,Input, NgZone } from '@angular/core';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { MatDialog } from '@angular/material';
import { DataService } from '../shared-services/data.service';
import { NgxSpinnerService } from "ngx-spinner";
import * as moment from 'moment';
import { Console } from 'console';
declare var $:any;
//declare var Razorpay:any;
declare var CollectJS:any;
// For Placeholder
var unserialize = require('locutus/php/var/unserialize');
declare var PerspT: any;

@Component({
  selector: 'app-exhibitor-booking',
  templateUrl: './exhibitor-booking.component.html',
  styleUrls: ['./exhibitor-booking.component.css']
})
export class ExhibitorBookingComponent implements OnInit {

  public imageObject = [
  {
    image: 'assets/img/slider/nine.png',
    thumbImage: 'assets/img/slider/nine.png',
    title: '9 SqM  (N 40,000)'
  },
  {
    image: 'assets/img/slider/tweleve.png',
    thumbImage: 'assets/img/slider/tweleve.png',
    title: '12 SqM  (N 50,000)'
  },
  {
    image: 'assets/img/slider/eighteen.png',
    thumbImage: 'assets/img/slider/eighteen.png',
     title: '18 SqM   (N 60,000)'
  },
  {
    image: 'assets/img/slider/twentyfour.png',
    thumbImage: 'assets/img/slider/twentyfour.png',
    title: '24 SqM  (N 70,000)'
  },
  {
    image: 'assets/img/slider/thirtysix.png',
    thumbImage: 'assets/img/slider/thirtysix.png',
    title: '36 SqM  (N 80,000)'
  },
  ]

  constructor(
    private service:DataService,
    public dialog: MatDialog){}

  ngOnInit(): void {

    window.scrollTo(0, 1);
    $.getScript('assets/js/count_down.js');
    
  }
      

}

import { Directive, Output, EventEmitter } from '@angular/core';

@Directive({
  selector: '[appNgInit]'
})
export class NgInitDirective {

  @Output()
  appNgInit: EventEmitter<any> = new EventEmitter();

  constructor() {
  
   }
   ngOnInit() {
    this.appNgInit.emit();
  }

}

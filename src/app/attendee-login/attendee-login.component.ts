import { Component, OnInit,ChangeDetectorRef,AfterViewInit,HostListener,Input, Inject} from '@angular/core';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { DataService } from '../shared-services/data.service';
import { ToastrService } from 'ngx-toastr';
import { Router,  ActivatedRoute } from '@angular/router';
import * as moment from 'moment';

declare var $:any;
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-attendee-login',
  templateUrl: './attendee-login.component.html',
  styleUrls: ['./attendee-login.component.css']
})
export class AttendeeLoginComponent implements OnInit {
  Attendee_Login_Form: FormGroup;
  Attendee_Forgot_Password_Form: FormGroup;
  Attendee_Change_Password_Form: FormGroup;

  public loading: boolean = false;
  videoUrl;
  imageUrl;
  userToken;
  logInForm = true;
  forgotPasswordForm = false;
  resetPasswordForm = false;

  constructor(
      private FB: FormBuilder,
      private service: DataService,
      private toastr: ToastrService,
      private router: Router,
      private activatedRoute: ActivatedRoute,
      private ref: ChangeDetectorRef,
      @Inject(DOCUMENT) private document: Document
  )
  {
    this.service.getRecordsNoFilter('get_design_event?type=entrance&event_id=1').subscribe(event_res =>
    {
      if (event_res.Status && event_res.Data != null && event_res.Data[0] != null)
      {
        this.service.getRecordsByid('boothtemplate',event_res.Data[0].layout_id).subscribe(res =>
        {
            if (res.Status && res.Data != '' && res.Data != null)
            {

                if (res.Data[0].media[0].media_type == 'video')
                {
                    this.videoUrl = res.Data[0].media[0].media_name;
                }

                if (res.Data[0].media[0].media_type == 'image')
                {
                    this.imageUrl = res.Data[0].media[0].media_name;
                }
                this.ref.markForCheck();
            }
        });
      }
    });
  }

  ngOnInit()
  {
      this.Attendee_Login_Form = this.FB.group(
      {
          email: ['', [Validators.email, Validators.required]],
          password: ['', [Validators.required, Validators.minLength(3)]],
      });

      this.Attendee_Forgot_Password_Form = this.FB.group(
      {
          forgotEmail: ['', [Validators.email, Validators.required]],
      });

      this.Attendee_Change_Password_Form = this.FB.group(
      {
          password: ['',
              Validators.compose([
                  Validators.required,
                  // Validators.email,
                  // Validators.minLength(3),
                  // Validators.maxLength(320)
              ])
          ],
          confirm_password: ['',
              Validators.compose([
                  Validators.required
              ])
          ]
      },
      {
          validator: this.mustMatch
      });

      if (this.activatedRoute.snapshot.queryParams['token'])
      {
          $('#exampleModal1').modal('show');

          this.activatedRoute.queryParams.subscribe(params =>
          {
              const userId = params['token'];
              if (userId)
              {
                  this.userToken = userId;
                  this.logInForm = true;
                  this.resetPasswordForm = true;
                  $(window).on('load', function()
                  {
                      $('#exampleModal1').modal(
                      {
                          backdrop: 'static',
                          keyboard: false
                      });
                  });
              }
          });
      }
  }

  mustMatch(group: FormGroup)
  {
      let passControl = group.get('password');
      let confirmPassControl = group.get('confirm_password');
      if (confirmPassControl.value)
      {
          if (passControl.value === confirmPassControl.value)
          {
              if (confirmPassControl.hasError('notSame'))
              {
                  delete confirmPassControl.errors['notSame'];
                  confirmPassControl.updateValueAndValidity();
              }
              return null;
          }
          else
          {
              confirmPassControl.setErrors(
              {
                  notSame: true
              });
              return {
                  notSame: true
              };
          }
      }
  }

  Attendee_Login()
  {
      if (this.Attendee_Login_Form.invalid)
      {
          return false;
      }

      this.loading = true;
      this.service.login('login', this.Attendee_Login_Form.controls['email'].value, this.Attendee_Login_Form.controls['password'].value).subscribe(user =>
      {
          if (user)
          {
              localStorage.setItem('accessToken', user.access_token);
              this.service.getProfile().subscribe(res =>
              {
                  if (res.type !== null)
                  {
                      if (res.type == 'attendee')
                      {
                          localStorage.setItem('role', res.type);
                          this.toastr.success("Login Successful", 'Success',
                          {
                              timeOut: 3000,
                              progressBar: true,
                              closeButton: true
                          });
                          this.loading = false;
                          this.document.location.href = 'https://webizexpo.com/virtual-expo/attendee/lobby';
                      }
                      else
                      {
                          this.toastr.error("Invalid User name and password", 'Error',
                          {
                              timeOut: 3000,
                              progressBar: true,
                              closeButton: true
                          });
                          this.loading = false;
                      }
                  }
              });
          }
      }, error =>
      {
          this.toastr.error("Invalid User name and password", 'Error',
          {
              timeOut: 3000,
              progressBar: true,
              closeButton: true
          });
          this.loading = false;
      });
  }


  forgotPassword()
  {
      this.logInForm = false;
      this.forgotPasswordForm = true;
  }

  forgotMailSend()
  {
      if (this.Attendee_Forgot_Password_Form.invalid)
      {
          return false;
      }
      const email = this.Attendee_Forgot_Password_Form.controls['forgotEmail'].value;
      this.loading = true;
      this.service.postRecord('passwordreset',
      {
          'email': email
      }).subscribe(res =>
      {
          if (res.Status)
          {
              this.toastr.success(res.Msg, 'Success',
              {
                  timeOut: 3000,
                  progressBar: true,
                  closeButton: true
              });
              this.logInForm = true;
          }
          else
          {
              this.toastr.error(res.Msg, 'Error',
              {
                  timeOut: 3000,
                  progressBar: true,
                  closeButton: true
              });
          }
          this.loading = false;
          this.forgotPasswordForm = false;
          this.ref.markForCheck();
      });
  }

  changePassword()
  {
      if (this.Attendee_Change_Password_Form.invalid)
      {
          return false;
      }
      this.loading = true;
      const postData = {
          'password': this.Attendee_Change_Password_Form.controls['password'].value,
          'token': this.userToken
      }
      this.service.postRecord('passwordchange', postData).subscribe(res =>
      {
          if (res.Status)
          {
              this.toastr.success(res.Msg, 'Success',
              {
                  timeOut: 3000,
                  progressBar: true,
                  closeButton: true
              });
              $('#exampleModal1').modal('hide');
              this.router.navigate(['/attendee-login']);
          }
          else
          {
              this.toastr.error(res.Msg, 'Error',
              {
                  timeOut: 3000,
                  progressBar: true,
                  closeButton: true
              });
          }
          this.loading = false;
          this.ref.markForCheck();
      });
  }
  forgotToLogin(){
    this.forgotPasswordForm = false;
    this.logInForm = true;
  }
}



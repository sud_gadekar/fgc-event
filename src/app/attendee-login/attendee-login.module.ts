import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// import { AttendeeLoginComponent } from './attendee-login.component';
import { ToastrModule } from 'ngx-toastr';
import { RouterModule } from '@angular/router';
import { FormsModule,ReactiveFormsModule }   from '@angular/forms';
@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class AttendeeLoginModule { }

import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomePageComponent } from './home-page/home-page.component';

import { SliderModule } from 'angular-image-slider';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DataService } from './shared-services/data.service';
import { HttpClientModule } from '@angular/common/http';
import { ToastrModule } from 'ngx-toastr';
import {AngularStickyThingsModule} from '@w11k/angular-sticky-things';
import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';
import {
	MatInputModule,
	MatPaginatorModule,
	MatProgressSpinnerModule,
	MatSortModule,
	MatTableModule,
	MatSelectModule,
	MatMenuModule,
	MatProgressBarModule,
	MatButtonModule,
	MatCheckboxModule,
	MatDialogModule,
	MatTabsModule,
	MatNativeDateModule,
	MatCardModule,
	MatRadioModule,
	MatIconModule,
	MatDatepickerModule,
	MatExpansionModule,
	MatAutocompleteModule,
	MAT_DIALOG_DEFAULT_OPTIONS,
	MatSnackBarModule,
	MatTooltipModule
} from '@angular/material';
import { EmbedVideo } from 'ngx-embed-video';
import { RegisterSuccessComponent } from './register-success/register-success.component';
import { AttendeeLoginComponent } from './attendee-login/attendee-login.component';
import { StoreModule } from '@ngrx/store';
import { NgImageSliderModule } from 'ng-image-slider';
import { LoadingBarModule } from '@ngx-loading-bar/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { NgbProgressbarModule } from '@ng-bootstrap/ng-bootstrap';
import { SponsorsComponent } from './sponsors/sponsors.component';
import { NgxSpinnerModule } from "ngx-spinner";
import { RecaptchaSettings, RECAPTCHA_SETTINGS } from 'ng-recaptcha';


@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
	RegisterSuccessComponent,
	AttendeeLoginComponent,
	SponsorsComponent
	
  ],
  imports: [
  	AngularStickyThingsModule,
  	HttpClientModule,
  	ToastrModule.forRoot(),
  	ScrollToModule.forRoot(),
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    SliderModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
	 MatPaginatorModule,
	 NgxSpinnerModule,
	MatProgressSpinnerModule,
	MatSortModule,
	MatTableModule,
	MatSelectModule,
	MatMenuModule,
	MatProgressBarModule,
	MatButtonModule,
	MatCheckboxModule,
	MatDialogModule,
	MatTabsModule,
	MatNativeDateModule,
	MatCardModule,
	MatRadioModule,
	MatIconModule,
	MatDatepickerModule,
	MatExpansionModule,
	MatAutocompleteModule,
	MatSnackBarModule,
	MatTooltipModule,
	EmbedVideo.forRoot(),
	NgImageSliderModule,
	NgbModule,
	LoadingBarModule,
	NgbProgressbarModule,
  ],
  providers: [DataService,
	{
    provide: RECAPTCHA_SETTINGS,
    useValue: {
      siteKey: '6LfDZtAZAAAAAHKHMMglFkyZqev6xlJgdjdBoa2C',
    } as RecaptchaSettings,
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }

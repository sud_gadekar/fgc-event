import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import {
	HttpClient,HttpHeaders} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class DataService {

  	//Online Api
	//baseUrl = "https://cannabizfair.com/virtual-expo-api/api/";	
	//Online Api AWS
	baseUrl = "https://webizexpo.com/virtual-expo-api/api/";	
	//Local Api
	//baseUrl="http://localhost/virtual-expo-api/api/";
	
	private loading = new BehaviorSubject(false);
	loadingExport = this.loading.asObservable();

	constructor(private httpService: HttpClient) {
		if (window.location.href.includes('leometric')) {
			this.baseUrl = "https://webizexpo.com/leometric/virtual-expo-api/api/";
		}else if(window.location.href.includes('localhost') && false) {
			this.baseUrl = "https://localhost/virtual-expo-api/api/";
		}else{
			this.baseUrl = "https://webizexpo.com/virtual-expo-api/api/";
		}
	}


	login(url: string, email, password): Observable < any > {

		return this.httpService.post(this.baseUrl + url, {
			email: email,
			password: password
		});
	}

	isLoggedin() {
		let token = localStorage.getItem("accessToken");
		return token !== null;
	}

	getProfile(): Observable < any > {
	const httpHeaders = new HttpHeaders({
       Authorization: `Bearer ${localStorage.getItem('accessToken')}`
     });
	return this.httpService.get < any > (this.baseUrl + 'me',{ headers: httpHeaders});
	}

	getRecords(url: string, limit: number, page_no: number, column: string, order: string, search: string, byid ? : string): Observable < any > {

		return this.httpService.get(`${this.baseUrl+url}?limit=${limit}&page_no=${page_no}&column=${column}&order=${order}&search=${search}&${byid}`);
	}

	getRecordsNoFilter(url: string): Observable < any > {
		const httpHeaders = new HttpHeaders({
			Authorization: `Bearer ${localStorage.getItem('accessToken')}`
		  });
		return this.httpService.get(`${this.baseUrl + url}`,{ headers: httpHeaders});
	}

	getRecordsByid(url, id): Observable < any > {
		return this.httpService.get < any > (this.baseUrl + url + '/' + id);
	}

	postRecord(url: string, data: any): Observable < any > {
		return this.httpService.post < any > (this.baseUrl + url, data);
	}

	updateRecord(url: string, id: number, data: any): Observable < any > {
		return this.httpService.put < any > (this.baseUrl + url + '/' + id, data);
	}

	deleteRecord(url: string, id: number): Observable < any > {
		return this.httpService.delete < any > (this.baseUrl + url + '/' + id);
	}
}

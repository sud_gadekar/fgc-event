import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-sponsors',
  templateUrl: './sponsors.component.html',
  styleUrls: ['./sponsors.component.css']
})
export class SponsorsComponent implements OnInit {

  title:boolean=true;
  prestige:boolean=false;
  royal:boolean=false;
  associate:boolean=false;

  constructor( private spinner: NgxSpinnerService) { }

  ngOnInit() {
    window.scrollTo(0, 1);
  }

  openTitle(){
    this.title = true;
    this.prestige=false;
    this.royal=false;
    this.associate=false;
  }

  openPrestige(){
   this.prestige = true;
   this.title=false;
   this.royal=false;
   this.associate=false;
  }
  openRoyal(){
    this.royal = true;
    this.prestige=false;
    this.title=false;
    this.associate=false;
  }
  openAssociate(){
    this.associate = true;
    this.prestige=false;
    this.title=false;
    this.royal=false;
  }
}
